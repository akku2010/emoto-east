import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DemoPage } from './demo';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DemoPage,
  ],
  imports: [
    IonicPageModule.forChild(DemoPage),
    TranslateModule.forChild()
  ],
})
export class DemoPageModule {}
