import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReportsCollectionPage } from './reports-collection';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ReportsCollectionPage,
  ],
  imports: [
    IonicPageModule.forChild(ReportsCollectionPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class ReportsCollectionPageModule {}
