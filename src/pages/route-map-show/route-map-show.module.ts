import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RouteMapShowPage } from './route-map-show';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    RouteMapShowPage,
  ],
  imports: [
    IonicPageModule.forChild(RouteMapShowPage),
    TranslateModule.forChild()
  ],
})
export class RouteMapShowPageModule {}
